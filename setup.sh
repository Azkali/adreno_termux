#!/bin/bash
set -e

# Sources:
# - https://gxmersam.blogspot.com/p/getting-hardware-acceleration-on-termux.html
# - https://github.com/termux/x11-packages/issues/394

# TODO:
# - box86 + wine32
# - box64 + wine64
# - zsh preexec + zsh precmd setup - https://zsh.sourceforge.io/Doc/Release/Functions.html#Hook-Functions

_build() {
	cd $1

	case "$1" in
		"mesa")
			git checkout b8970120545b3cb250821013cb459bf4d2acfda4
			for patch in ../patches/mesa/*.patch; do patch -Np1 --ignore-whitespace --fuzz 3 < $patch; done
		;;
		"libepoxy")
			for patch in ../patches/epoxy/*.patch; do patch -Np1 --ignore-whitespace --fuzz 3 < $patch; done
		;;
		"xorg-server-1.20.13")
			# TODO: Fix os_utils patch which fails but applies...
			for patch in ../patches/xorg/*.patch; do patch -Np1 --ignore-whitespace --fuzz 3 < $patch || true; done
		;;
		*)
		;;
	esac

	echo -e "Building $1\n"
	if [[ ! -e "./autogen.sh" ]]; then
		meson build -Dprefix=$PREFIX "${@:2}"
		ninja -j8 -C build install
	else
		./autogen.sh --prefix=$PREFIX "${@:2}"

		[[ "$1" == "libxshmfence" ]] && \
			sed -i s/values.h/limits.h/ ./src/xshmfence_futex.h

		make -j8 install
	fi

	cd ..
}

setup() {
	echo -e "Installing required dependencies\n"
	pkg install -y x11-repo
	pkg install -y clang lld cmake autoconf automake libtool '*ndk*' make python git libandroid-shmem-static 'vulkan*' ninja llvm bison flex libx11 libdrm libpixman libxfixes libjpeg-turbo xtrans libxxf86vm xorg-xrandr xorg-font-util xorg-util-macros libxfont2 libxkbfile libpciaccess xcb-util-renderutil xcb-util-image xcb-util-keysyms xcb-util-wm xorg-xkbcomp xkeyboard-config libxdamage libxinerama binutils xfce4 xfce4-terminal
	pip install meson mako

	[[ ! -e ~/storage ]] && \
		termux-setup-storage

	echo -e "Cloning needed repositories\n"
	git clone --depth 1 git://github.com/freedesktop/xorg-xorgproto.git
	git clone --depth 1 git://github.com/wayland-project/wayland.git
	git clone --depth 1 git://github.com/wayland-project/wayland-protocols.git
	git clone --depth 1 git://github.com/freedesktop/libxshmfence.git
	git clone --single-branch --shallow-since 2021-02-22 git://github.com/mesa3d/mesa.git 
	git clone --depth 1 git://github.com/dottedmag/libsha1.git
	git clone --depth 1 git://github.com/anholt/libepoxy.git
	git clone -b xorg-server-1.20.13 --depth 1 git://github.com/freedesktop/xorg-xserver.git xorg-server-1.20.13
	git clone --depth 1 https://gitlab.freedesktop.org/mesa/drm
}

build() {
	_build xorg-xorgproto --with-xmlto=no --with-fop=no --with-xsltproc=no
	_build wayland -Ddocumentation=false
	_build wayland-protocols
	_build libxshmfence --with-shared-memory-dir=$TMPDIR CPPFLAGS=-DMAXINT=INT_MAX
	_build drm -Dfreedreno=true -Dfreedreno-kgsl=true -Dnouveau=false
	_build mesa # -Dplatforms=x11,wayland -Dgallium-drivers=swrast,zink,freedreno -Dvulkan-drivers=freedreno -Ddri3=enabled  -Degl=enabled  -Dgles2=enabled -Dglvnd=true -Dglx=dri  -Dlibunwind=disabled -Dosmesa=true  -Dshared-glapi=enabled -Dmicrosoft-clc=disabled  -Dvalgrind=disabled -Dgles1=disabled -Dfreedreno-kgsl=true -Dcpp_rtti=false
	_build libsha1
	_build libepoxy -Degl=yes -Dglx=yes -Dtests=false -Dx11=true
	_build xorg-server-1.20.13 --enable-mitshm --enable-xcsecurity --enable-xf86bigfont --enable-xwayland --enable-xorg --enable-xnest --enable-xvfb --disable-xwin --enable-xephyr --enable-kdrive --disable-devel-docs --disable-config-hal --disable-config-udev --disable-unit-tests --disable-selective-werror --disable-static --without-dtrace --disable-glamor --enable-dri --enable-dri2 --enable-dri3 --enable-glx --with-sha1=libsha1 --with-pic LDFLAGS='-fuse-ld=lld /data/data/com.termux/files/usr/lib/libandroid-shmem.a -llog'
}

setup
build
